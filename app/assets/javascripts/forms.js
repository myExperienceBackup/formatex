
$(document).ready(function(){
  var count = 1;
  $(function() {
    $( "#sortable" ).sortable({
      // revert: true,
      over: function () {
        removeIntent = false;
      },
      out: function () {
          removeIntent = true;
      },
      beforeStop: function (event, ui) {
          if(removeIntent == true){
              ui.item.remove();   
          }
        }
    });
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
  });
  $('#line_add').click(function(){
    var name = $(this).siblings('input').val().toLowerCase();
    $("#sortable").append('<li class="ui-state-default"><label for="input'+count+'">'+titleize(name)+'</label><br><input type="text" name="result['+name+ ']" size="40" id="input' + count + '" class="line"></li>');
    count++;
    $(this).siblings('input').val("");
    saveForm();
  });
  $('#text_add').click(function(){
    var name = $(this).siblings('input').val().toLowerCase();
    $("#sortable").append('<li class="ui-state-default"><label for="input'+count+'">'+titleize(name)+'</label><br><textarea name="result['+name+']"></textarea></li>');
    $(this).siblings('input').val("");
    saveForm();
  });  
  $('#select_add').click(function(){
    var name = $(this).siblings('input#label').val().toLowerCase();
    var text = $(this).siblings('input#options').val();
    var options = text.split(' ');
    var options_html = ""
    $.each(options, function(index) {
      options_html += ('<option value="'+ options[index].toLowerCase() +'">' + options[index] + '</option>');
    });
    console.log(options_html);
    $("#sortable").append('<li class="ui-state-default"><label for="'+name+'">'+titleize(name)+'</label><br><select name="result['+name+']">'+ options_html +'</select></li>');
    var name = $(this).siblings('input#label').val("");
    var text = $(this).siblings('input#options').val("");
    saveForm();
  });
  $('#radio_add').click(function(){
    var name = $(this).siblings('input#label').val().toLowerCase();
    var options = $(this).siblings('input#radioptions').val().split(' ');
    var options_html = ""
    $.each(options, function(index) {
      options_html += ('<input type="radio" name="result[' + name +']" value="'+options[index].toLowerCase()+'">' + options[index] + "<br>");
    });
    $("#sortable").append('<li class="ui-state-default"><label for="'+name+'">'+titleize(name)+'</label>'+ options_html +'</li>');
    $(this).siblings('input').val("");
    saveForm();
  });

  $('#hr_add').click(function(){
    $("#sortable").append('<li class="ui-state-default"><hr></li>');
    saveForm();
  });

  $('#clear').click(function(){
    $( "li" ).each(function( index ) {
      $( this ).slideUp( "slow", function() {
        $( this ).remove();
      });
    });
  });
  $('#header_input').keyup(function(){
    $('#header').text($(this).val());
    $('#form_title').val($(this).val());
    saveForm();
  });
  $('#header_color').keyup(function(){
    $('#header').css("background-color", $(this).val());
    saveForm();
  });
  $('#bg_color_input').keyup(function(){
    $('#main').css( "background-color", $(this).val() );
    saveForm();
  });
  $('#inp_color_input').keyup(function(){
    $('#sortable').css( "background-color", $(this).val() );
    saveForm();
  });
  
  $('.del').droppable({
      click: function(event, ui) {
          $(ui.draggable).remove();
      }
  });

  function saveForm() {
    console.log($("#main").html());
    $("#form_body").val($("#main").html());
  };
  function disable() { 
    $( "#sortable" ).sortable('disable');
  };

  function titleize(string) {
    return string[0].toUpperCase() + string.slice(1);
  };
  $('#trash').droppable({
      over: function(event, ui) {
          $(ui.draggable).remove();
      }
  });
  $("#line_size").change(function(){
    $("input[type=text]").last(".line").css('width', $(this).val()+"px");
  });
  $("#header_size").change(function(){
    console.log($(this).val());
    $("#header").css('font-size', $(this).val()+"px");
  });

  $( "#sortable" ).on( "sortchange", function() { saveForm();} );
});