json.array!(@results) do |result|
  json.extract! result, :id, :form_id, :integer
  json.url result_url(result, format: :json)
end
