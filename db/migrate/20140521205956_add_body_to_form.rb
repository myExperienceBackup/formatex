class AddBodyToForm < ActiveRecord::Migration
  def change
  	add_column :forms, :body, :string, default: ""
  end
end
