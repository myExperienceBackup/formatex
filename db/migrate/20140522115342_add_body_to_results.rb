class AddBodyToResults < ActiveRecord::Migration
  def change
  	add_column :results, :body, :string, default: ""
  end
end
