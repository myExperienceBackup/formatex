class AddTitleToForms < ActiveRecord::Migration
  def change
  	add_column :forms, :title, :string, default: "<nameless form>"
  end
end
