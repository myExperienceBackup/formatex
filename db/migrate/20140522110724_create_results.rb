class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :form_id
      t.timestamps
    end
  end
end
