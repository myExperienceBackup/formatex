class ChangeFormBodyType < ActiveRecord::Migration
  def change
  	change_column :forms, :body, :text, default: ""
  end
end
